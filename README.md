# README #

A more detailed README and source-code comments are coming soon.

### IMPORTANT ###

IF YOU USE THIS CODE IN PART OR WHOLE FOR ANY PURPOSE, please cite our paper on these simulations: 

Jasmine Brewer, Miller Mendoza, Ryan E. Young, and Paul Romatschke. "Lattice Boltzmann simulations of a two-dimensional Fermi gas at unitarity." arXiv:1507.05975. [http://arxiv.org/abs/1507.05975](). 

IF YOU USE THE CODE TO SIMULATE THE NON-IDEAL EQUATION OF STATE contained in the file EoS-2D-1.0.dat, cite:

Marianne Bauer, Meera M. Parish, and Tilman Enss. "Universal Equation of State and Pseudogap in the Two-Dimensional Fermi Gas." Phys. Rev. Lett. 112, 135302 (2014). [http://journals.aps.org/prl/abstract/10.1103/PhysRevLett.112.135302]()

### What is this repository for? ###

* This repository contains source code, input files, and compile information for the simulations on unitary Fermi gases as discussed in arXiv:1507.05975. Additional comments in the code are coming soon!
* Version 1

### How do I get set up? ###

* source code: 11-02-D2Q25omp.cpp
* compile script: compile.sh
* command-line input: tr008.txt
* file for ideal equation of state: input/EoS-2D-idF.dat
* file for non-ideal equation of state: input/EoS-2D-1.0.dat
* helper functions: linear_int, myspline, newtonguess
* example script to launch the application on a node on a distributed computing system: launch

### Who do I talk to? ###

* Jasmine Brewer, jtbrewer[at]mit[dot]edu