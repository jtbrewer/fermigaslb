
//this just returns the next iteration for root finding in a Newton scheme
double newtonguess(double xn, double fn, double dfn)
{
  double result=xn-fn/dfn; 
  return result;
}

//same as above, but with additional damping
double newtonguess(double xn, double fn, double dfn,double dd)
{
  double result=xn-fn/dfn*dd; 
  return result;
}
