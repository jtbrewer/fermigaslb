#!/bin/bash
g++ -I. -c linear_int.cpp -O3
g++ -I. -c newtonguess.cpp -O3
g++ 11-02-D2Q25omp.cpp linear_int.o newtonguess.o -fopenmp -I. -lgsl -lgslcblas -O3 -o SLB2DTeff
