
/** FermiGasLB, version 1 (released July 2014)
**********************************************

Authors: Jasmine Brewer, Paul Romatschke, and Ryan E. Young
Reference: arXiv:1507.05975 (2015)

IF YOU USE THE NON-IDEAL EQUATION OF STATE for any purpose, including as an input to this code, cite:
Marianne Bauer, Meera M. Parish, and Tilman Enss. "Universal Equation of State and Pseudogap in the Two-Dimensional Fermi Gas." Phys. Rev. Lett. 112, 135302 (2014)

**********************************************
**/

//  Edited and expanded by PR, Aug 2013 & Oct 2014

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <locale>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <sys/stat.h>
#include <omp.h>
#include <linear_int.h>
#include <newtonguess.h>
#include <myspline.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_sf.h>

#define Q 25
#define D 2

char * outdir;
//initial gaussian deformations
 double defx=1.0; 
 double defy=1.0;

int NEWTONSTEPS=10;

//#define harmonic 1
#define EOS 1
//#define ramp 1
//#define period 1
#define gauss 1
//#define phasein 1

#ifdef ramp
double sramp=0.001;
#else
double sramp=0;
#endif 


#ifdef gauss
//V0/T0, e.g. potential over reference temperature
double vot=10;
#endif

double Teff=1.;
int verbose=0;//write output files to directory 1-yes 0-no

myspline Pressure,density,invdens;
double b2; //second viral coefficient for EoS

double Ifugfromn(double n,double T);

using namespace std;

double potential(int nx, int ny, double lambda, double defx, double defy, double cl2, double sd,int i,int j)
{
  double middlex = nx/2;
  double middley = ny/2;
  double res;
#ifdef harmonic
  res=0.5*((i-middlex)*(i-middlex)*defx+(j-middley)*(j-middley)*lambda*lambda*defy)/(sd*sd*cl2);
#elif period
  double xx=2*M_PI/nx*(i-middlex);
  double yy=2*M_PI/ny*(j-middley);
  //not currently working
  //res=pow(nx/(2*M_PI*sd),2)*(1-cos((i-middlex)*2*M_PI/nx))+pow(ny/(2*M_PI*sd),2)*(1-cos((j-middley)*2*M_PI/ny));
  //res=(pow(nx/(2*M_PI*sd),2)*(1.25-4./3.*cos((i-middlex)*2*M_PI/nx)+1./12.*cos(2*(i-middlex)*2*M_PI/nx))+pow(ny/(2*M_PI*sd),2)*(1.25-4./3.*cos((j-middley)*2*M_PI/ny)+1./12.*cos(2*(j-middley)*2*M_PI/ny)));
  //res=(pow(nx/(2*M_PI*sd),2)*(49/36.-3./2.*cos((i-middlex)*2*M_PI/nx)+3./20.*cos(2*(i-middlex)*2*M_PI/nx)-1./90*cos(3*(i-middlex)*2*M_PI/nx))+pow(ny/(2*M_PI*sd),2)*(49/36.-3./2.*cos((j-middley)*2*M_PI/ny)+3./20.*cos(2*(j-middley)*2*M_PI/ny)-1./90*cos(3*(j-middley)*2*M_PI/ny)));
  //res=nx*nx*(1./12.-1/(M_PI*M_PI)*cos(xx))+ny*ny*(1./12.-1/(M_PI*M_PI)*cos(yy));
  res=(pow(nx/sd,2)*(1./12.-1/(M_PI*M_PI)*cos(xx))+lambda*pow(ny/sd,2)*(1./12.-1/(M_PI*M_PI)*cos(yy)))/(2*cl2);
#elif gauss
  res=vot*(1-exp(-(i-middlex)*(i-middlex)*defx/(vot*2*sd*sd*cl2)-(j-middley)*(j-middley)*lambda*lambda*defy/(vot*2*sd*sd*cl2)));
#endif
  return res;
}

double dxpotential(int nx, int ny, double lambda, double sd,double cl2, int i,int j)
{
  double middlex = nx/2;
  double middley = ny/2;
  double res;
#ifdef harmonic
  res=((i-middlex)/sd);
#elif period
  double xx=2*M_PI/nx*(i-middlex);
  double yy=2*M_PI/ny*(j-middley);
  //res=nx/(2*M_PI*sd)*sin((i-middlex)*2*M_PI/nx);
  //res=nx/(2*M_PI*sd)*(4./3.*sin((i-middlex)*2*M_PI/nx)-1./6.*sin(2*(i-middlex)*2*M_PI/nx));
  //res=nx/(2*M_PI*sd)*(3./2.*sin((i-middlex)*2*M_PI/nx)-2*3./20.*sin(2*(i-middlex)*2*M_PI/nx)+3./90*sin(3*(i-middlex)*2*M_PI/nx));
  res=nx/(sd*M_PI)*sin(xx);
#elif gauss
  res=(i-middlex)/sd*exp(-(i-middlex)*(i-middlex)/(vot*2*sd*sd*cl2)-(j-middley)*(j-middley)*lambda*lambda/(vot*2*sd*sd*cl2));
#endif
  return res;
}

double dypotential(int nx, int ny, double lambda, double sd,double cl2, int i,int j)
{
  double middlex = nx/2;
  double middley = ny/2;
  double res;
#ifdef harmonic
  res=lambda*lambda*((j-middley)/sd);
#elif period
  double yy=(j-middley)*2*M_PI/ny;
  //res=ny/(2*M_PI*sd)*sin((j-middley)*2*M_PI/ny);
  //res=ny/(2*M_PI*sd)*(4./3.*sin((j-middley)*2*M_PI/ny)-1./6.*sin(2*(j-middley)*2*M_PI/ny));
  //res=ny/(2*M_PI*sd)*(3./2.*sin(yy)-2*3./20.*sin(2*yy)+3./90*sin(3*yy));
  res=ny/(sd*M_PI)*sin(yy);
#elif gauss
  res=lambda*lambda*((j-middley)/sd)*(exp(-(i-middlex)*(i-middlex)/(vot*2*sd*sd*cl2)-(j-middley)*(j-middley)*lambda*lambda/(vot*2*sd*sd*cl2)));
#endif
  return res;
}

double n(double T,double MU)
{
  double Ifug=exp(MU/T);
  double result;
#ifdef EOS 
  //ideal fermi gas
  //result=log(1+Ifug);
  if (MU/T>density.low)
    result=density.f(MU/T);
  else
    result=Ifug*(1+2*b2*log(1+Ifug))/(1+Ifug);
#else
  //ideal gas
  result=Ifug;
#endif
  return result;
}

double Ifugfromn(double n,double T)
{
  double result=0;
#ifdef EOS 
  //ideal fermi gas
  //result=exp(n/T)-1;
  if (n/T>invdens.low)
    result=exp(invdens.f(n/T));
  else
    result=(-1+n/T+sqrt(1-2*n/T+8*b2*n/T+n*n/(T*T)))/(4*b2); //neglecting second virial correction, hopefully OK
#else
  result=n;
  #endif
  return result;
}

double dIfugfromn(double n,double T)
{
  double result=0;
  double nt=n/T;
#ifdef EOS 
  //ideal fermi gas
  //result=exp(n/T);
  if (nt>invdens.low)
    result=exp(invdens.f(nt))*invdens.df(nt);
  else
    result=(1+(-1+4*b2+nt)/sqrt(1-2*nt+8*b2*nt+nt*nt))/(4*b2); //neglecting second virial correction, hopefully OK
#else
  result=n;
  #endif
  return result;
}


double PovernT(double Ifug)
{
  //double Ifug=exp(MU/T);
  double muot=log(Ifug);
  double result;
#ifdef EOS
  //ideal fermi gas
  //result=-gsl_sf_dilog(-Ifug)/(log(1+Ifug));
  if (muot>density.low)
    result=Pressure.f(muot);
  else
    result=1+b2*log(1+Ifug);
#else
  //ideal gas
  result=1;
#endif
  return result;
}

double dPovernT(double Ifug)
{
  double muot=log(Ifug);
  double result;
#ifdef EOS
  //result=1/Ifug-gsl_sf_dilog(-Ifug)/(log(1+Ifug)*log(1+Ifug)*(1+Ifug));
  if (muot>density.low)
    result=Pressure.df(muot)*Ifug;
  else
    result=b2/(1+Ifug);
#else
  result=0;
#endif
  return result;
}


void init_gaussian(vector<vector<vector <double> > > * fIn,vector<vector<vector <double> > > * fOut,vector<vector <double> > * rho, vector<vector <double> > * ux, vector<vector <double> > * uy, double c[Q][D], double wi[Q],double lambda, int nx, int ny, double sd, double cl2, double omega,double muT) {

  double middlex = nx/2;
  double middley = ny/2;  

  // double def = 1.0810175;
  // sd = sd*def;
  //srand(1235);

  for (int i = 0; i < nx ; i++) {
    for (int j = 0; j < ny ; j++) {

      Teff=sqrt(defx*defy);
      double mu=muT*Teff-potential(nx,ny,lambda,defx,defy,cl2,sd,i,j);
      
      (*rho)[i][j] = n(Teff,mu);
      //exp( -potential(nx,ny,lambda,defx,defy,cl2,sd,i,j))*sqrt(defx*defy);

       
       double u_sqr=0;
      // (*rho)[i][j] = exp( -(0.5/cl2)*((i-middlex)/sd)*((i-middlex)/sd) - (0.5/cl2)*lambda*lambda*((j-middley)/sd)*((j-middley)/sd) );

      for (int n = 0; n < Q; n++)
	{
	  double c_dot_u =0;
	  double c_sqr = (c[n][0])*(c[n][0]) + (c[n][1])*(c[n][1]);
	  
	  //(*fIn)[i][j][n] = wi[n]*((*rho)[i][j])*( 1.0 + (c_dot_u/cl2)*(1.0 + ((Teff-1.0)/(2.0*cl2))*(c_sqr - 4.0*cl2)) + (c_dot_u*c_dot_u)/(2.0*cl2*cl2) - u_sqr/(2.0*cl2) + ((Teff-1.0)/(2.0*cl2))*(c_sqr-2.0*cl2) + (c_dot_u*c_dot_u*c_dot_u)/(6.0*cl2*cl2*cl2) - (u_sqr*c_dot_u)/(2.0*cl2*cl2) +sramp*(rand()%100-100));
	  double Ifug=exp(mu/Teff);

	  (*fIn)[i][j][n] = wi[n]*((*rho)[i][j])*( 1.0 + (c_dot_u/cl2)*(1.0 + ((Teff*PovernT(Ifug)-1.0)/(2.0*cl2))*(c_sqr - 4.0*cl2)) + (c_dot_u*c_dot_u)/(2.0*cl2*cl2) - u_sqr/(2.0*cl2) + ((Teff*PovernT(Ifug)-1.0)/(2.0*cl2))*(c_sqr-2.0*cl2) + (c_dot_u*c_dot_u*c_dot_u)/(6.0*cl2*cl2*cl2) - (u_sqr*c_dot_u)/(2.0*cl2*cl2));


	  (*fOut)[i][j][n] = (*fIn)[i][j][n];
	
	}
    }
  }


  double r = 0;
  double uxx = 0;
  double E = 0;

  for (int n=0; n<Q; n++) {

    r = r+(*fIn)[6][23][n];
    uxx = uxx+((*fIn)[6][23][n])*(c[n][0]);
    E = E+((*fIn)[6][23][n])*((c[n][0])*(c[n][0]) + (c[n][1])*(c[n][1]));
  }
  cout << "init-gaussian: r = " << r << ", ux = " << uxx << ", E = " << E << endl;
}

int pBCx(int in, int nx) { // periodic boundaries in x

  if (in > nx-1 || in < 0) {
    in = (in+nx)%nx;
  }
  return in;
}

int pBCy(int jn, int ny) { // periodic boundaries in y

  if (jn > ny-1 || jn < 0) {
    jn = (jn+ny)%ny;
  }
  return jn;
}

double minmod(double a, double b) {

  double res = 0.0;
  if ( a*b>0.0 ) {

    if ( fabs(a)<fabs(b) ) {
      res = a;
    } else {
      res = b;
    }
  }
  return res;
}

double fluxlim( vector<vector<vector <double> > > * fOut, double c[Q][D], int i, int j, int n, int nx, int ny, double dt) {

  int ip1, in;

  double f000 = (*fOut)[i][j][n];
  // if (i==nx/2 && j==ny/2 && n==0) {
  //   cout << "fOut = " << (*fOut)[i][j][n] << endl;
  // }
  double fp, fm, fpp, fmm, dfdx, dfdy, fluxplus, fluxminus, sigma, temp;
 
  if (c[n][0] != 0) {

    if (c[n][0] < 0) {

      ip1 = pBCx( i+1,nx );

      fp = (*fOut)[ pBCx( i+1 ,nx) ][ j ][n];
      fm = (*fOut)[ pBCx( i-1 ,nx) ][ j ][n];
      fpp = (*fOut)[ pBCx( i+2 ,nx) ][ j ][n];

      sigma = minmod( f000-fm, fp-f000 );
      fluxminus = f000 - 0.5*sigma;

      sigma = minmod( fp-f000, fpp-fp );
      fluxplus = fp - 0.5*sigma;

    }
    else { // cx(n) > 0

      fp = (*fOut)[ pBCx( i+1 ,nx) ][ j ][n];
      fm = (*fOut)[ pBCx( i-1 ,nx) ][ j ][n];
      fmm = (*fOut)[ pBCx( i-2 ,nx) ][ j ][n];

      sigma = minmod( fm-fmm, f000-fm );
      fluxminus = fm + 0.5*sigma;

      sigma = minmod( f000-fm, fp-f000 );
      fluxplus = f000 + 0.5*sigma;
    }
  }

  else { // cx(n) = 0

    fluxplus = 0.0;
    fluxminus = 0.0;
  }

  dfdx = fluxplus - fluxminus;

  if (c[n][1] != 0) {

    if (c[n][1] < 0) {

      fp = (*fOut)[i][ pBCy( j+1, ny ) ][n];
      fm = (*fOut)[i][ pBCy( j-1, ny ) ][n];
      fpp = (*fOut)[i][ pBCy( j+2, ny ) ][n];

      sigma = minmod( f000-fm, fp-f000 );
      fluxminus = f000 - 0.5*sigma;

      sigma = minmod( fp-f000, fpp-fp );
      fluxplus = fp - 0.5*sigma;

    }
    else { // cy(n) > 0

      fp = (*fOut)[i][ pBCy( j+1, ny ) ][n];
      fm = (*fOut)[i][ pBCy( j-1, ny ) ][n];
      fmm = (*fOut)[i][ pBCy( j-2, ny ) ][n];

      sigma = minmod( fm-fmm, f000-fm );
      fluxminus = fm + 0.5*sigma;

      sigma = minmod( f000-fm, fp-f000 );
      fluxplus = f000 + 0.5*sigma;
    }
  }

  else { // cy(n) = 0

    fluxplus = 0.0;
    fluxminus = 0.0;
  }

  dfdy = fluxplus - fluxminus;

  temp = f000 - dt*( (c[n][0])*dfdx + (c[n][1])*dfdy );

  return temp;
}

void calc_expvalues(vector<vector<vector <double> > > * fIn, vector<vector <double> > * expvalues,double c[Q][D], int nx, int ny, double lambda, double sd, int ts ) {

  double expx = 0.0;
  double expy = 0.0;
  double expvx = 0.0;
  double expvy = 0.0;
  double x,y;

  double middlex = nx/2;
  double middley = ny/2;

  double wx = 1;
  double wy = lambda;

  for (int i=0; i<nx; i++) {
    for (int j=0; j<ny; j++) {

      x = (i-middlex)/sd;
      y = (j-middley)/sd;

      for (int n=0; n<Q; n++) {

      expx = expx + x*x*((*fIn)[i][j][n]);
      expy = expy + y*y*((*fIn)[i][j][n]);

      expvx = expvx + (c[n][0])*(c[n][0])*((*fIn)[i][j][n]);
      expvy = expvy + (c[n][1])*(c[n][1])*((*fIn)[i][j][n]);

      }
    }
  }

  double size = double(nx)*double(ny)*Q;

  double diffx = wx*wx*(expx/size) - expvx/size;
  double diffy = wy*wy*(expy/size) - expvy/size;

  (*expvalues)[ts][0] = diffx;
  (*expvalues)[ts][1] = diffy;

  // cout << "<x^2> = " << expx/size << ", <y^2> = " << expy/size << ", <vx^2> = " << expvx/size << "<vy^2> = " << expvy/size << endl;

  // cout << "diffx = " << diffx / (expx/size) << ", diffy = " << diffy / (expy/size) << endl;
}

void eq_and_stream(vector<vector<vector <double> > > * fIn,vector<vector<vector <double> > > * fOut, vector<vector <double> > * rho, vector<vector <double> > * ux, vector<vector <double> > * uy, double c[Q][D], double wi[Q], double lambda, int nx, int ny, double cl2, double omega, double sd, int ftrue, double dt, double ca) {


  double u_sqr, c_dot_u, force;
  int in, jn;
  double x, y;
  
  double check_rho, check_ux, check_uy;

  double middlex = nx/2;
  double middley = ny/2;

  double c_sqr;

  double cdotX,udotX;

  double Ptot=0,Ntot=0;
  
  int NUM_THREADS=1;
  /*Disable dynamic allocation of threads*/
  //omp_set_dynamic(0);
  
  /*Allocate threads*/
  //omp_set_num_threads(NUM_THREADS);

  double E;
  int i,j,n;
  long int position;
  long int sit0=0,sit1=0;
  //double myscal=0.5;
  long int sites=0;

  int tid;

  double Ifug;
  double checkme=0;
  double dcheckme=0;
  
  //Teff=1;
  sites=0;
  //calculate global values
#pragma omp parallel shared(rho,ux,uy,Ptot,Ntot,checkme,dcheckme,fIn,fOut,c,sites) private(x,y,i,j,n,position,u_sqr,Ifug)
  {

    tid = omp_get_thread_num();
    int nthreads = omp_get_num_threads();
    //printf("===> Info: Number of threads = %d\n", nthreads);
    
#pragma omp for schedule(dynamic) reduction(+:Ntot,Ptot,checkme,dcheckme)
    for (position=0;position<nx*ny;position++)
      {

      i=position%nx;
      j=position/nx;
      
      //printf("i=%i j=%i\n",i,j);

      //x = (i-middlex)/sd;
      //y = lambda*lambda*(j-middley)/sd;
      x=dxpotential(nx,ny,lambda,sd,cl2,i,j);
      y=dypotential(nx,ny,lambda,sd,cl2,i,j);


      //set to zero before summing
      (*rho)[i][j]=0.0;
      (*ux)[i][j]=0.0;
      (*uy)[i][j]=0.0;
	            
      for (n = 0; n < Q; n++)
	{	  
	  (*rho)[i][j] = (*rho)[i][j]+(*fIn)[i][j][n]; // rho
	  (*ux)[i][j] = (*ux)[i][j]+(c[n][0])*((*fIn)[i][j][n]); // ux
	  (*uy)[i][j] = (*uy)[i][j]+(c[n][1])*((*fIn)[i][j][n]); // uy
	  Ptot+=(*fIn)[i][j][n]*( (c[n][0])*(c[n][0])+(c[n][1])*(c[n][1]) );
	}

      (*ux)[i][j] = (*ux)[i][j]/(*rho)[i][j];
      (*uy)[i][j] = (*uy)[i][j]/(*rho)[i][j];

      if (ftrue == 1) {
	
      	(*ux)[i][j] = (*ux)[i][j] - 0.5*x/sd;
      	(*uy)[i][j] = (*uy)[i][j] - 0.5*y/sd;
      }     
      Ntot+=(*rho)[i][j];
      u_sqr = ((*ux)[i][j])*((*ux)[i][j]) + ((*uy)[i][j])*((*uy)[i][j]);
      Ptot+=-(*rho)[i][j]*(u_sqr);
      //Ifug=exp(((*rho)[i][j])/Teff)-1;
      //checkme+=((*rho)[i][j])*PovernT(Ifug);
      //dcheckme-=((*rho)[i][j])*((*rho)[i][j])*dPovernT(Ifug);
      }
  }

  
  /*
  for (i=0; i<nx; i++) 
    for (j=0; j<ny; j++) 
      {
	for (n = 0; n < Q; n++)
	  Ptot+=(*fIn)[i][j][n]*( (c[n][0])*(c[n][0])+(c[n][1])*(c[n][1]) );
	u_sqr = ((*ux)[i][j])*((*ux)[i][j]) + ((*uy)[i][j])*((*uy)[i][j]);
	Ptot-=(*rho)[i][j]*(u_sqr);
	//Ntot+=(*rho)[i][j];
	}*/
  
  
  //printf("tot en =%f density =%f T=%f \n",Ptot,Ntot,Ptot/Ntot/(2*cl2));

  double stp=0.125;

  for (int iter=0;iter<NEWTONSTEPS;iter++)
    {
      checkme=0;
      dcheckme=0;
#pragma omp parallel shared(rho,ux,uy,Ptot,Ntot,checkme,dcheckme,fIn,fOut,c,sites) private(x,y,i,j,n,position,u_sqr,Ifug)
      {
#pragma omp for schedule(dynamic) reduction(+:checkme,dcheckme)
	for (position=0;position<nx*ny;position++)
	  {
	    i=position%nx;
	    j=position/nx;
	    Ifug=Ifugfromn((*rho)[i][j],Teff);
	    checkme+=((*rho)[i][j])*PovernT(Ifug);
	    dcheckme+=((*rho)[i][j])*((*rho)[i][j])*dPovernT(Ifug)*dIfugfromn((*rho)[i][j],Teff);
	  }
      }
          
      //printf("Teff=%f where ptot/(2cl)=%f Ntot=%f, checkme=%f, dcheckme=%f\n",Teff,Ptot/(2*cl2),Ntot,checkme,dcheckme);
      //printf("original %f\n",Teff);
      Teff=newtonguess(Teff,Ptot/(2*cl2)-Teff*checkme,-checkme+dcheckme/Teff,stp);
      
      //printf("Teff=%f\n",Teff);
      
    }

  //Teff=Ptot/Ntot/(2.0*cl2);
  //Teff = 1.0;
  

  double check_E;
  
  double fEq[Q];
  
      

#pragma omp parallel shared(rho,ux,uy,Ptot,Ntot,fIn,fOut,c,sites,ftrue) private(x,y,i,j,n,position,u_sqr,c_dot_u,c_sqr,fEq,cdotX,udotX,force,Ifug)
  {

    tid = omp_get_thread_num();
    //printf("===> Info: Number of threads = %d\n", nthreads);
    
#pragma omp for schedule(dynamic)
    for (position=0;position<nx*ny;position++)
      {

      i=position%nx;
      j=position/nx;
      
      //x = (i-middlex)/sd;
      //y = lambda*lambda*(j-middley)/sd;
      x=dxpotential(nx,ny,lambda,sd,cl2,i,j);
      y=dypotential(nx,ny,lambda,sd,cl2,i,j);
      //Ifug=exp(((*rho)[i][j])/Teff)-1;
      Ifug=Ifugfromn((*rho)[i][j],Teff);
      u_sqr = ((*ux)[i][j])*((*ux)[i][j]) + ((*uy)[i][j])*((*uy)[i][j]);

      for (n = 0; n < Q; n++) {

	c_dot_u = (c[n][0])*((*ux)[i][j]) + (c[n][1])*((*uy)[i][j]);

	c_sqr = (c[n][0])*(c[n][0]) + (c[n][1])*(c[n][1]);

	

	fEq[n] = wi[n]*((*rho)[i][j])*( 1.0 + (c_dot_u/cl2)*(1.0 + ((Teff*PovernT(Ifug)-1.0)/(2.0*cl2))*(c_sqr - 4.0*cl2)) + (c_dot_u*c_dot_u)/(2.0*cl2*cl2) - u_sqr/(2.0*cl2) + ((Teff*PovernT(Ifug)-1.0)/(2.0*cl2))*(c_sqr-2.0*cl2) + (c_dot_u*c_dot_u*c_dot_u)/(6.0*cl2*cl2*cl2) - (u_sqr*c_dot_u)/(2.0*cl2*cl2) ); // newer

	

	cdotX = (c[n][0])*x + (c[n][1])*y;
	udotX = x*((*ux)[i][j]) + y*((*uy)[i][j]);

	if (ftrue == 1) {
	  
	  //force = -(1.0-0.5*omega)/sd*wi[n]*((*rho)[i][j])*(cdotX/cl2-udotX/cl2)*(1.0 + ((Teff-1.0)/(2.0*cl2))*(c_sqr - 4.0*cl2)+c_dot_u/cl2+ (c_dot_u*c_dot_u)/(2.0*cl2*cl2) - u_sqr/(2.0*cl2));//my form
	  force = -(1.0-0.5*omega*Teff)/sd*wi[n]*((*rho)[i][j])*(
							    cdotX/cl2*(1+c_dot_u/cl2+(c_dot_u*c_dot_u)/(2.0*cl2*cl2)-u_sqr/(2.0*cl2)+0.5*(Teff*PovernT(Ifug)-1.0)*(c_sqr/cl2 - 4.0))
							    -udotX/cl2*(1+c_dot_u/cl2));
	  
	  
	}
	else if (ftrue == 0) {
	  force = 0.0;
	  if (i==0 && j==0 && n==0) {
	    cout << "potential is off" << endl;
	  }
	}
	else {
	  cout << "ftrue should be either zero or one, please and thanks." << endl;
	}

	
	(*fIn)[i][j][n] = ((*fIn)[i][j][n])*(1.0-omega*Teff) + omega*Teff*fEq[n] + force;
      }

      
      }
  }

#pragma omp parallel shared(rho,ux,uy,Ptot,Ntot,fIn,fOut,c,sites,ftrue) private(x,y,i,j,n,position,u_sqr,c_dot_u,c_sqr,fEq,cdotX,udotX,force,in,jn)
  {
    
#pragma omp for schedule(dynamic)
    for (position=0;position<nx*ny;position++)
      {
	
	i=position%nx;
	j=position/nx;
	for (n=0; n<Q; n++) 
	  {
	    in = i + int(c[n][0]);
	    jn = j + int(c[n][1]);
	    
	    in = pBCx( in,nx );
	    jn = pBCy( jn,ny );
	    
	    (*fOut)[in][jn][n] = (*fIn)[i][j][n];
	  }
      }
  }


  
}

void getbs(vector<vector <double> > * rho, vector<vector <double> > * ux, vector<vector <double> > * uy, int nx, int ny, double sd, int ts,double cl2, double *bx,double *by,double *vx,double *vy)
{

  int middlex = nx/2;
  int middley = ny/2;
  float sinv=1.0/sd;
  double dcen=(*rho)[middlex][middley];
  int i=middlex;
  while((i<nx-1)&&((*rho)[i][middley]>dcen*exp(-1)))
    {
      i++;
    }
  
  //*bx=(i-middlex)*sinv*sqrt(1/(2*cl2));
  *bx=linear_int((*rho)[i-1][middley],(*rho)[i][middley],(i-middlex-1)*sinv*sqrt(1/(2*cl2)),(i-middlex)*sinv*sqrt(1/(2*cl2)),dcen*exp(-1));
  *vx=(*ux)[i][middley];
  
  i=middley;
  while((i<ny-1)&&((*rho)[middlex][i]>dcen*exp(-1)))
    {
      i++;
    }
  //*by=(i-middley)*sinv*sqrt(1/(2*cl2));
  *by=linear_int((*rho)[middlex][i-1],(*rho)[middlex][i],(i-middley-1)*sinv*sqrt(1/(2*cl2)),(i-middley)*sinv*sqrt(1/(2*cl2)),dcen*exp(-1));
  *vy=(*uy)[middlex][i];

  printf("bx =%f vx=%f by=%f vy=%f\n",*bx,*vx,*by,*vy);
}


double getenergy(vector<vector<vector <double> > > * fIn,vector<vector <double> > * rho, vector<vector <double> > * ux, vector<vector <double> > * uy, double c[Q][D], double lambda,int nx, int ny, double sd, int ts,double cl2,double *tn,double *tp, double *te)
{

  //double tn = 0.0;
  //double ke = 0.0;
  //double pe = 0.0;
  *tn=0;
  tp[0]=0;
  tp[1]=0;
  *te=0;
  
  for (int i=0; i<nx; i++) {
    for (int j=0; j<ny; j++) {
      for (int n=0; n<Q; n++) {
	double x = (i-nx/2)/sd;
	double y = lambda*lambda*(j-nx/2)/sd;
	*tn+=(*fIn)[i][j][n];
	tp[0]+=(c[n][0])*(*fIn)[i][j][n];
	tp[1]+=(c[n][1])*(*fIn)[i][j][n];
	*te+=( (c[n][0])*(c[n][0])+(c[n][1])*(c[n][1]) +x*x+y*y)*(*fIn)[i][j][n];

	//pe = pe + ( (c[n][0]-ux[i][j])*(c[n][0]-ux[i][j])+(c[n][1]-uy[i][j])*(c[n][1]-uy[i][j]) )*(fIn[i][j][n]);

	//	en = en + ( (c[n][0])*(c[n][0])+(c[n][1])*(c[n][1]) )*( fIn[i][j][n] );

	//ke = ke + ( (ux[i][j])*(ux[i][j]) + (uy[i][j])*(uy[i][j]) )*( fIn[i][j][n] );

	 }
         }
     }
  *tn=(*tn)/(sd*sd*cl2);
  //energy[ts] = en;
  //KE[ts] = ke;
  //PE[ts] = pe;
  return 0;
  }



void write_gaussian(vector<vector<vector <double> > > * fIn, vector<vector <double> > * rho, vector<vector <double> > * ux, vector<vector <double> > * uy, int nx, int ny, double sd, int ts) {

  fstream out;
  char fname[255];
  float sinv=1.0/sd;
  int middlex = nx/2;
  int middley = ny/2;

  sprintf(fname,"%s/Xrho_t%i.dat",outdir,ts);
  out.open(fname, ios::out);
  for(int i=0; i < nx; i++){
    int j=middley;
    out << (i-middlex)*sinv << "\t";
    out << (*rho)[i][j] << "\n";
  }  
        
  out.close(); 

  sprintf(fname,"%s/Yrho_t%i.dat",outdir,ts);
  out.open(fname, ios::out);
  for(int j=0; j < ny; j++){
    int i=middlex;
    out << (j-middley)*sinv << "\t";
    out << (*rho)[i][j] << "\n";
  }  
        
  out.close(); 

  sprintf(fname,"%s/2drho_t%i.dat",outdir,ts);
  out.open(fname, ios::out);
  for(int i=0; i < nx; i++)
    for(int j=0; j < ny; j++)
      {
	out << (i-middlex)*sinv << "\t";
	out << (j-middley)*sinv << "\t";
	out << setprecision(15) << (*rho)[i][j] << "\n";
      }

  out.close(); 


  sprintf(fname,"%s/Xux_t%i.dat",outdir,ts);
  out.open(fname, ios::out);
  for(int i=0; i < nx; i++){
    int j=middley;
    out << (i-middlex)*sinv << "\t";
    out << setprecision(15) << (*ux)[i][j] << "\n";
  }  
        
  out.close(); 

   sprintf(fname,"%s/Yux_t%i.dat",outdir,ts);
  out.open(fname, ios::out);
  for(int j=0; j < ny; j++){
    int i=middlex+50;
    out << (j-middley)*sinv << "\t";
    out << setprecision(15) << (*ux)[i][j] << "\n";
  } 
        
  out.close();  

  sprintf(fname,"%s/Yuy_t%i.dat",outdir,ts);
  out.open(fname, ios::out);
  for(int j=0; j < ny; j++){
    int i=middlex;
    out << (j-middley)*sinv << "\t";
    out << setprecision(15) << (*uy)[i][j] << "\n";
  } 
        
  out.close();  

  sprintf(fname,"%s/2dux_t%i.dat",outdir,ts);
  out.open(fname, ios::out);
  for(int i=0; i < nx; i++)
    for(int j=0; j < ny; j++)
      {
	out << (i-middlex)*sinv << "\t";
	out << (j-middley)*sinv << "\t";
	out << setprecision(15) << (*ux)[i][j] << "\n";
      }
  
  out.close();  

  sprintf(fname,"%s/2duy_t%i.dat",outdir,ts);
  out.open(fname, ios::out);
  for(int i=0; i < nx; i++)
    for(int j=0; j < ny; j++)
      {
	out << (i-middlex)*sinv << "\t";
	out << (j-middley)*sinv << "\t";
	out << setprecision(15) << (*uy)[i][j] << "\n";
      }
  
  out.close();  

}

//load equation of state from input file
void loadeos(char *EOSNAME)
{
  fstream eosf;

  char eosfile[255];
  sprintf(eosfile,"input/%s.dat",EOSNAME);
  //sprintf(eosfile,"input/%s.dat","EoS-2D-2.0");
  //sprintf(eosfile,"input/%s.dat","EoS-2D-idF");
  eosf.open(eosfile,ios::in);
  double *mu,*nn,*PP;
  int length;
  if (eosf.is_open())
    {
      eosf >> length;
      eosf >> b2;
      mu=new double[length];
      nn=new double[length];
      PP=new double[length];
      for (int i=1;i<=length;i++)
	{
	  eosf >> mu[i-1];
	  eosf >> nn[i-1];
	  eosf >> PP[i-1];
	}
      eosf.close();

      printf("===> Info: Successfully loaded EoS file of length %i\n",length);

      Pressure.alloc(mu,PP,length);
      density.alloc(mu,nn,length);
      invdens.alloc(nn,mu,length);

      //printf(" TEST: %f and %f\n",Pressure.f(0.),density.f(0.));
      //printf(" TEST2: mu/T=%f=%f, %f %f \n",-1.1,log(Ifugfromn(n(1,-1.1),1)),n(1,-1.1),invdens.low);

      cout<< "density.low = " << density.low << endl;

    }
  else
    cout << "Could not open EOS file" << endl; 

}

//THIS IS STANDARD LB, NOT MINMOD!!!!
int main(int argc, const char * argv[])
{
   char dummychar[255];
   int dummyint;
   double dummydouble;     

   //printf("this is a test %f\n",PovernT(1.,0.5));

  double sig1 = 50.0;
  //double omega1 = 1.0;

  int nx = 251;
  int ny = 251;
  int nsteps = 250;
  double sd = 50.0;
  double omega = 0.2;
  double omega0;
  double ca = 1.0; // factor by which to scale the velocities
  char EOSNAME[255];
  double Toverhw,muT;

  double tauR=1.0;

  // omega = omega/ca;

  double dt = 1.0;
  double START=0;
  int meas_steps = 10;

  double dtreal = 5; // micro-seconds
  double wxreal = 0.0319104; // micro-Hz
  int dtsim = round( dtreal*wxreal*sd/dt );
  // double lambda = 0.0345955;
  double lambda = 1.0;
  int t_off = 10*(sd/sig1)/dt; // time step to turn potential off
  int t_on = t_off + dtsim; // time step to turn it back on

  int ABORT=0;
  
  char *paramsfile;
  

  if (argc>1)
    {
      paramsfile=(char *)argv[1];
    }
  else
    {
      sprintf(dummychar,"%s","params.txt");
      paramsfile=dummychar;
    }

  fstream parameters;
  parameters.open(paramsfile, ios::in);
  if(parameters.is_open())
    {      
      parameters >> dummychar;
      parameters >> dummyint;
      nx=dummyint;
      parameters >> dummychar;
      parameters >> dummyint;
      ny=dummyint;
      parameters >> dummychar;
      parameters >> dummydouble;
      sd=dummydouble;
      parameters >> dummychar;
      parameters >> dummyint;
      nsteps=dummyint;
#ifdef phasein
      START=nsteps/2;
#else
      START=0;
#endif
      parameters >> dummychar;
      parameters >> dummyint;
      meas_steps=dummyint;
      parameters >> dummychar;
      parameters >> dummydouble;
      tauR=dummydouble;
      parameters >> dummychar;
      parameters >> dummydouble;
      defx=dummydouble;
      parameters >> dummychar;
      parameters >> dummydouble;
      defy=dummydouble;    
      parameters >> dummychar;
      parameters >> dummyint;
      verbose=dummyint;
      parameters >> dummychar;
      parameters >> dummydouble;
      lambda=dummydouble; 
      parameters >> dummychar;
      parameters >> dummychar;
      sprintf(EOSNAME,"%s",dummychar);
      parameters >> dummychar;
      parameters >> dummydouble;
      Toverhw=dummydouble;
      parameters >> dummychar;
      parameters >> dummydouble;
      muT=dummydouble;
      parameters >> dummychar;
      parameters >> dummyint;
      NEWTONSTEPS=dummyint;
    }
  else
    {
      printf("\nERROR: params file params.txt could not be opened\n");
      ABORT=1;
    }
  parameters.close();

  if (!ABORT)
    {

      printf("Running 2d simulation for nx=%i ny=%i sd=%f tauR=%f\n",nx,ny,sd,tauR);
      
      char cbuffer[1000];
      char abuffer[1000];
      char dbuffer[1000];
 
      
#ifdef harmonic
      sprintf(abuffer,"%s","har");
#elif period
      sprintf(abuffer,"%s","per");
#elif gauss
      sprintf(abuffer,"%s%.1f","gauss",vot);
#endif

#ifdef ramp
      sprintf(dbuffer,"%s%f","-ran",sramp);
#elif EOS
      sprintf(dbuffer,"-%s-mut%f",EOSNAME,muT);
      loadeos(EOSNAME);
#else
      sprintf(dbuffer,"%s","");
#endif

      sprintf(cbuffer,"%i-%i-sd%.1f-tR%.2f-lam%.3f-dfx%.6f-dfy%.6f-%s%s",nx,ny,sd,tauR,lambda,defx,defy,abuffer,dbuffer);
      // sprintf(cbuffer,"data");
      outdir=cbuffer;
      

      //test if out directory exists, if not create it
      struct stat st;
      if (stat(outdir,&st) != 0)
	{
	  printf("===> Info: directory %s not present, creating it\n",outdir);
	  mkdir(outdir,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	}
      sprintf(dummychar,"cp %s %s",paramsfile,outdir);
      dummyint=system(dummychar);
      
      //correct for numerical viscosity
      omega=1/(0.5+tauR*sd);
#ifdef phasein
      omega0=omega;
#endif
      
      //int ftrue =0; // 1 if potential on, 0 if potential off
       int ftrue = 1;
      
      double u_sqr, c_dot_u, force;
      int in, jn;
      
      double cl2= 1.0-sqrt(2.0/5.0); //lattice speed squared
      
      vector<vector<vector <double> > > fIn (nx, vector<vector <double> >(ny, vector <double>(Q,0.0)));
      
      vector<vector<vector <double> > > fOut (nx, vector<vector <double> >(ny, vector <double>(Q,0.0)));
      
      vector<vector <double> > rho (nx, vector <double> (ny,1.0));
      vector<vector <double> > ux (nx, vector <double> (ny,0.0));
      vector<vector <double> > uy (nx, vector <double> (ny,0.0));
      
      double c[Q][D] = {{0.0,0.0},{1.0,0.0},{-1.0,0.0},{0.0,1.0},{0.0,-1.0},{1.0,1.0},{-1.0,-1.0},{1.0,-1.0},{-1.0,1.0},{3.0,0.0},{-3.0,0.0},{0.0,3.0},{0.0,-3.0},{3.0,3.0},{-3.0,-3.0},{3.0,-3.0},{-3.0,3.0},{1.0,3.0},{-1.0,-3.0},{1.0,-3.0},{-1.0,3.0},{3.0,1.0},{-3.0,-1.0},{3.0,-1.0},{-3.0,1.0}};
      
      for (int n=0; n<Q; n++) {
	for (int d=0; d<D; d++) {
	  
	  c[n][d] = int(ca)*c[n][d];
	  
	}
      }
      
      // int nop[Q] = {0,3,4,1,2,7,8,5,6};
      
      double w0 = (4.0/45.0)*( 4.0+sqrt(10.0) );
      double w1 = (3.0/80.0)*( 8.0-sqrt(10.0) );
      double w3 = (1.0/720.0)*( 16.0-5.0*sqrt(10.0) );
      
      double wi[] = {w0*w0,w0*w1,w0*w1,w0*w1,w0*w1,w1*w1,w1*w1,w1*w1,w1*w1,w0*w3,w0*w3,w0*w3,w0*w3,w3*w3,w3*w3,w3*w3,w3*w3,w1*w3,w1*w3,w1*w3,w1*w3,w1*w3,w1*w3,w1*w3,w1*w3};
      
      init_gaussian(&fIn,&fOut,&rho,&ux,&uy,c,wi,lambda,nx,ny,sd,cl2,omega,muT);   
      
      // scale cl2 by some factor
      cl2 = cl2*ca*ca;
      
      double bx=0;
      double by=0;
      double vx=0;
      double vy=0;
      
      double tn=0,te=0;
      double tp[2];
      
      fstream bvals,cons;
      char buffer[255];
      sprintf(buffer,"%s/bval.dat",outdir);
      bvals.open(buffer,ios::out);
      bvals << "#Time bx-by bx+by vx vy n(center) Teff\n";
      
      sprintf(buffer,"%s/cons.dat",outdir);
      cons.open(buffer,ios::out);
      cons << "#time density momentum energy\n";

#ifdef phasein
      omega=1/(0.5+0.1*sd);
#endif
      cout << "omega = " << omega << ", dt = " << dt << ", cl2 = " << cl2 << endl;
      getenergy(&fIn,&rho,&ux,&uy,c,lambda, nx,ny,sd,0,cl2,&tn,tp,&te);

     // printf("Total particle number %f\n",tn/(2*M_PI)*(Toverhw*Toverhw));
      cout << "Total particle number: " << tn/(2*M_PI)*(Toverhw*Toverhw) << endl;

      for (int ts=0; ts < nsteps; ts++) {
	/*
	if (ts == t_off) {
	  ftrue = 0;
	}
	if (ts == t_on) {
	  ftrue = 1;
	  }*/
	
	// if (ts == t_off) {
	//   ftrue = 1;
	//   //omega=0.1*dt;
	// }
	
	// if (ts == t_on) {
	//   ftrue = 1;
	//   //omega=omega0*dt;
	//   //lambda=1.0;
	// }
	
	eq_and_stream(&fIn,&fOut,&rho,&ux,&uy,c,wi,lambda,nx,ny,cl2,omega,sd,ftrue,dt,ca);
	
	fIn = fOut;  
	
	if ( ts%20==0 ) {
	  for (int i=0; i<nx; i++) {
	    for (int j=0; j<ny; j++) {
	      for (int n=0; n<Q; n++) {
		
		if (isnan(fIn[i][j][n])) {
		  cout << "Terminating -- nans!" << endl;
		  return -1;
		}
	      }
	    }
	  }
	}
#ifdef phasein
	if (ts%60==0)
	  {
	    if (omega>omega0)
	      omega*=0.99;
	    cout << "t=" << ts*dt/sd << "\t omega = " << omega << endl;
	  }
	if (ts == START-1)
	  lambda=1.0;
#endif
	if ((ts%meas_steps == 0)) {
	  //printf("meas\n");
	  if (verbose)
	    write_gaussian(&fIn,&rho,&ux,&uy,nx,ny,sd,ts);
	  //printf("ok, next\n");
	  getbs(&rho,&ux,&uy,nx,ny,sd,ts,cl2,&bx,&by,&vx,&vy);
	  printf("Teff=%f \n",Teff);
	  getenergy(&fIn,&rho,&ux,&uy,c,lambda, nx,ny,sd,ts,cl2,&tn,tp,&te);
	  bvals << setprecision(10) << (ts-START)*dt/(sd) << "\t" << bx-by << "\t" << bx+by << "\t" << vx << "\t" << vy << "\t" << rho[nx/2][ny/2] << "\t" << Teff << "\n";
	  cons << setprecision(10) << (ts-START)*dt/sd <<  "\t" << tn << "\t" << tp[0] <<"\t" << tp[1] << "\t" << te << "\n";
	  bvals.flush();
	  cons.flush();
	  //printf("finished all\n");
	}
      }
      bvals.close();
      bvals.close();
    }
  return 0;
}
